jQuery( document ).ready( function( $ ) {
    function move_slider( pointer_location, object ) {
        var mouse_location = pointer_location - $( object ).offset().left;
        $( object ).find( ".overlay" ).css( "width", mouse_location + "px" );
    }

	// setup
	$( ".image_slider" ).each( function() {
		var images = $( this ).find( "img" );
		if( images.length >= 2 ) {
			// work out how high to make it
			var x_width = Math.max( $( images[ 0 ] ).width(), parseInt( $( images[ 0 ] ).attr( "width" ) ), parseInt( $( images[ 0 ] ).css( "width" ).replace( "px", "" ) ) );
			var y_width = Math.max( $( images[ 1 ] ).width(), parseInt( $( images[ 1 ] ).attr( "width" ) ), parseInt( $( images[ 1 ] ).css( "width" ).replace( "px", "" ) ) );
			var x_height = Math.max( $( images[ 0 ] ).height(), parseInt( $( images[ 0 ] ).attr( "height" ) ), parseInt( $( images[ 0 ] ).css( "height" ).replace( "px", "" ) ) );
			var y_height = Math.max( $( images[ 1 ] ).height(), parseInt( $( images[ 1 ] ).attr( "height" ) ), parseInt( $( images[ 1 ] ).css( "height" ).replace( "px", "" ) ) );
			var max_width = Math.min( x_width, y_width );
			var max_height = Math.min( x_height, y_height );

			$( this ).height( max_height ).width( max_width );

			// find image urls
			var x_src = $( images[ 0 ] ).attr( 'src' );
			var y_src = $( images[ 1 ] ).attr( 'src' );

			// re-structure div
			$( this ).html( '<div class="overlay"></div>' );
			$( this ).css( "backgroundImage", 'url(' + x_src + ')' );
			$( this ).find( ".overlay" ).css( "backgroundImage", 'url(' + y_src + ')' ).height( max_height ).width( '50%' );
		
			// account for smaller screen on mobile
			var aspect_ratio = max_height / max_width;
			$( this ).resize( function() {
				if( $( this ).width() < max_width - 1 ) {
					$( this ).height( aspect_ratio * $( this ).width() );
					$( this ).find( ".overlay" ).height( aspect_ratio * $( this ).width() );
				}
			} );
			setInterval( function() {
				$( ".image_slider" ).trigger( $.Event( "resize" ) );
			}, 1000 );
		}

		// adjust slider
		$( this ).mousemove( function( event ) {
			move_slider( event.pageX, this );
		} );
		$( this ).on( "touchmove", function( event ) {
			var start_position = event.originalEvent.touches[ 0 ].pageX;
			move_slider( start_position, this ) 
		} );
		$( this ).on( "touchend", function( event ) {
			var position = event.originalEvent.changedTouches[ 0 ].pageX	
			move_slider( position, this );
		} );
	} );
} );
