<?php
/*
Plugin Name: Image Sliders
Plugin URI: http://www.imge.com
Description: Reddit style image 'sliders' where two images are overlayed and % seen of each is controlled by the mouse.
Version: 1.0
Author: Chris Lewis
Author URI: http://www.imge.com
*/

function image_scripts() {
    wp_enqueue_style( 'image-stylesheet', plugin_dir_url( __FILE__ ) . "image.css" );
    wp_enqueue_script( 'image-script', plugin_dir_url( __FILE__ ) . 'image.js', array( 'jquery' ), '', true );

}
add_action( 'wp_enqueue_scripts', 'image_scripts' );

function slider_shortcode( $atts, $content = null ) {
    return '<div class="image_slider">' . $content . '</div>';
}
add_shortcode( 'image_slider', 'slider_shortcode' );

?>
